package pl.sda.service;

import pl.sda.exception.MessageNotFoundException;
import pl.sda.model.Message;

import java.util.ArrayList;
import java.util.List;

public class MessageService {

    private List<Message> messages = new ArrayList<>();

    public MessageService() {

        messages.add(new Message(1, "message-1", null));
    }

    public void addMessage(Message message) {
        messages.add(message);
    }

    public List<Message> getAllMessages() {
        return messages;
    }

    public void updateMessage(Message message) throws MessageNotFoundException {

        int index = getIndexById(message.getId());

        if (index == -1) {
            throw new MessageNotFoundException("Message with id "
                    + message.getId() + " not found");
        }

        messages.get(index).setText(message.getText());

    }

    public void deleteMessage(Integer id) throws MessageNotFoundException {
        Message message = getMessageById(id);

        if (message == null) {
            throw new MessageNotFoundException("Message with id "
                    + id + " not found");
        }

        messages.remove(message);
    }

    public Message getMessageById(Integer id) throws MessageNotFoundException {

        for (Message message : messages) {
            if (message.getId().equals(id)) {
                return message;
            }
        }

        throw new MessageNotFoundException("Message with id "
                + id + " not found");
    }

    private int getIndexById(Integer id) {
        for (int i=0; i<messages.size(); i++) {
            if (messages.get(i).getId().equals(id)) {
                return i;
            }
        }

        return -1;
    }

}