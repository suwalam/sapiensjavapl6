package pl.sda.resources;

import lombok.extern.java.Log;
import pl.sda.exception.MessageNotFoundException;
import pl.sda.model.Message;
import pl.sda.service.MessageService;

import javax.ws.rs.*;
import javax.ws.rs.core.*;
import java.util.Arrays;

@Log
@Path("/messages")
public class MessageResource {

    private MessageService messageService = new MessageService();

    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getAllMessages() {
        log.info("Returned all messages!");
        return Response.status(200)
                .entity(messageService.getAllMessages())
                .build();
    }

    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getMessageById(@PathParam("id") Integer id) throws MessageNotFoundException {
        log.info("Returned message with id " + id);
        return Response.status(200)
                .entity(messageService.getMessageById(id))
                .build();
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @POST
    public Response createMessage(Message message) {
        log.info("Added new message: " + message);

        messageService.addMessage(message);

        return Response.status(Response.Status.CREATED).build();
    }

    @Consumes(MediaType.APPLICATION_JSON)
    @Produces(MediaType.APPLICATION_JSON)
    @PUT
    public Response updateMessage(Message message) throws MessageNotFoundException {
        log.info("Updated message: " + message);

        messageService.updateMessage(message);

        return Response.status(Response.Status.NO_CONTENT).build();
    }

    @DELETE
    @Path("/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    public Response deleteMessage(@PathParam("id") Integer id) throws MessageNotFoundException {
        log.info("Deleted message with id " + id);
        messageService.deleteMessage(id);
        return Response.status(Response.Status.OK).build();
    }

    @Path("/hateoas/{id}")
    @Produces(MediaType.APPLICATION_JSON)
    @GET
    public Response getMessageByIdHATEOAS(@PathParam("id") Integer id, @Context UriInfo uriInfo) throws MessageNotFoundException {
        log.info("Returned message by id with HATEOAS " + id);
        Message messageById = messageService.getMessageById(id);
        Link selfLink = Link.fromUriBuilder(uriInfo.getAbsolutePathBuilder()).rel("self").build();

        if (messageById != null) {
            messageById.setLinks(Arrays.asList(selfLink));
        }

        return Response.status(Response.Status.OK)
                .entity(messageById)
                .links(selfLink)
                .build();
    }

}
