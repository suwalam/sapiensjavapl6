package pl.sda.resources;


import javax.ws.rs.GET;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

public class HelloWorldResource {

    @Produces(MediaType.APPLICATION_JSON)
    @Path("/hello")
    @GET
    public Response helloWorld() {
        return Response.status(Response.Status.OK)//status HTTP 200
                .entity("Hello World!")
                .build();
    }
}
