package pl.sda.model;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.ws.rs.core.Link;
import java.util.List;

@NoArgsConstructor
@AllArgsConstructor
@Data
public class Message {

    private Integer id;

    private String text;

    private List<Link> links;

}
