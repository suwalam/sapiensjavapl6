package pl.sda.config;

import pl.sda.resources.HelloWorldResource;
import pl.sda.resources.MessageResource;

import javax.ws.rs.ApplicationPath;
import javax.ws.rs.core.Application;
import java.util.HashSet;
import java.util.Set;

@ApplicationPath("/api")
public class RestConfig extends Application {

    //rejestracja klas HelloWorldResource i MessageResource jako potrafiącej obsługiwać żądania HTTP
    @Override
    public Set<Object> getSingletons() {
        Set<Object> singletons = new HashSet<>();
        singletons.add(new HelloWorldResource());
        singletons.add(new MessageResource());
        return singletons;
    }
}
