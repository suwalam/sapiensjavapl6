package pl.sda.filter;

import lombok.extern.java.Log;

import javax.ws.rs.container.ContainerRequestContext;
import javax.ws.rs.container.ContainerRequestFilter;
import javax.ws.rs.ext.Provider;
import java.io.IOException;

@Log
@Provider
public class PrintHeadersRequestFilter implements ContainerRequestFilter {

    @Override
    public void filter(ContainerRequestContext ctx) throws IOException {

        log.info("Handled request with method: " + ctx.getMethod());

        ctx.getHeaders()
                .entrySet()
                .stream()
                .forEach(e -> log.info(e.getKey() + " - " + e.getValue()));

    }
}
