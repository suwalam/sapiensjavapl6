package pl.sda.resources;

import org.glassfish.jersey.test.JerseyTest;
import org.junit.Test;
import pl.sda.config.RestConfig;
import pl.sda.model.Message;

import javax.ws.rs.core.Application;
import javax.ws.rs.core.Response;
import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertNotNull;

public class MessageResourceTest extends JerseyTest {

    @Override
    protected Application configure() {
        return new RestConfig();
    }

    @Test
    public void shouldGetAllMessages() {
        //given
        String endpoint = "/messages";

        //when
        Response response = target(endpoint).request().get();

        //then
        assertEquals(200, response.getStatus());
        assertNotNull(response.getEntity());
    }

}